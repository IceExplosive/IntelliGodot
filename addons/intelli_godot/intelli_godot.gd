@tool
extends EditorPlugin

var script_editor: ScriptEditor
var control: Control
var editor: EditorInterface

const IDE_EXE = "idea64.exe"
const PROJECT_PATH = "C:\\Users\\david\\Desktop\\IntelliGodot"

const PID_TXT = "intelli_godot_pid.txt";
const SETTINGS_TXT = "intelli_godot_settings.txt";

const ACTIVATE_AHK = "activate.ahk";
const DEACTIVATE_AHK = "deactivate.ahk";
const MOVE_AHK = "move.ahk";
const SHOW_AHK = "show.ahk";
const HIDE_AHK = "hide.ahk";

const PID_TXT_FOR_AHK = "addons/intelli_godot/%s" % PID_TXT
const SETTINGS_TXT_FOR_AHK = "addons/intelli_godot/%s" % SETTINGS_TXT

const WS_SIZEBOX: int = 0x00040000;
const WS_CAPTION: int = 0x00C00000;

var scripts
var hidden := false;
var pid := ""

func _enter_tree() -> void:
	scripts = PROJECT_PATH + "\\addons\\intelli_godot\\"
	
	editor = get_editor_interface()
	control = editor.get_base_control()
	script_editor = editor.get_script_editor()
	
	_init_scripts();
	script_editor.resized.connect(_move);
	script_editor.visibility_changed.connect(_visibility);
	_move();
	_visibility();


func _exit_tree() -> void:
	script_editor.resized.disconnect(_move);
	script_editor.visibility_changed.disconnect(_visibility);
	_deactivate();
	script_editor = null
	editor = null
	control = null
	pass


func _visibility() -> void:
	if script_editor.visible:
		OS.shell_open(scripts + SHOW_AHK);
	elif !script_editor.visible:
		OS.shell_open(scripts + HIDE_AHK);
	hidden = !script_editor.visible;


func _move() -> void:
	var file := FileAccess.open(scripts + MOVE_AHK, FileAccess.WRITE);
	var size := script_editor.get_global_rect();
	var flags := 0;
	if hidden: flags = 0x0010;
	file.store_line("DllCall(\"SetWindowPos\", \"uint\", %s, \"uint\", 0, \"int\", %s, \"int\", %s, \"int\", %s, \"int\", %s, \"int\", %s)" % [
		pid, size.position.x, size.position.y, size.size.x, size.size.y, flags
	]);
	OS.shell_open(scripts + MOVE_AHK);


func _pid() -> String:
	var file = FileAccess.open(scripts + PID_TXT, FileAccess.READ);
	return file.get_line();


func _deactivate() -> void:
	var file := FileAccess.open(scripts + DEACTIVATE_AHK, FileAccess.WRITE);
	file.store_line("DllCall(\"SetParent\", \"uint\", %s, \"uint\", 0x00010010)" % pid);
	file.store_line("DllCall(\"SetWindowLong\", \"ptr\", %s, \"int\", -16, \"int\", %s | 0x00C40000& ~0x40000000)" % [pid, _settings()]);
	OS.shell_open(scripts + DEACTIVATE_AHK);


func _init_scripts() -> void:
	var file := FileAccess.open(scripts + ACTIVATE_AHK, FileAccess.WRITE);
	file.store_line("WinGet, WChild_ID , ID, ahk_exe idea64.exe");
	file.store_line("DllCall(\"SetParent\", \"uint\", WChild_ID, \"uint\", %s)" % DisplayServer.window_get_native_handle(DisplayServer.WINDOW_HANDLE));
	file.store_line("IntelliStyle := DllCall(\"GetWindowLong\", \"ptr\", WChild_ID, \"int\", -16)");
	file.store_line("DllCall(\"SetWindowLong\", \"uint\", WChild_ID, \"int\", -16, \"int\", IntelliStyle | 0x40000000 & ~0x80C40000)");
	file.store_line("FileDelete, %s" % PID_TXT_FOR_AHK);
	file.store_line("FileDelete, %s" % SETTINGS_TXT_FOR_AHK);
	file.store_line("FileAppend, %%WChild_ID%%, %s" % PID_TXT_FOR_AHK);
	file.store_line("FileAppend, %%IntelliStyle%%, %s" % SETTINGS_TXT_FOR_AHK);
	
	OS.shell_open(scripts + ACTIVATE_AHK);
	await get_tree().create_timer(0.5).timeout
	pid = _pid();
	
	file = FileAccess.open(scripts + SHOW_AHK, FileAccess.WRITE);
	file.store_line("DllCall(\"ShowWindowAsync\", \"uint\", %s, \"int\", 1)" % pid);
	
	file = FileAccess.open(scripts + HIDE_AHK, FileAccess.WRITE);
	file.store_line("DllCall(\"ShowWindowAsync\", \"uint\", %s, \"int\", 0)" % pid);


func _settings() -> String:
	var file = FileAccess.open(scripts + SETTINGS_TXT, FileAccess.READ);
	return file.get_line();

